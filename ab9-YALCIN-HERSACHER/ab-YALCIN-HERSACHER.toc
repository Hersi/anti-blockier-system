\contentsline {section}{\numberline {1}Aufgabenstellung und Zielsetzung}{2}
\contentsline {section}{\numberline {2}Versuchsaufbau}{3}
\contentsline {section}{\numberline {3}Modellierung}{5}
\contentsline {subsection}{\numberline {3.1}Antrieb des Motors}{5}
\contentsline {subsection}{\numberline {3.2}Servomotor Ansteuerung zum Bremsen}{5}
\contentsline {subsection}{\numberline {3.3}Die Interrupts}{5}
\contentsline {section}{\numberline {4}Implementierung}{6}
\contentsline {subsection}{\numberline {4.1}Pin-Belegung der Komponenten}{6}
\contentsline {subsubsection}{\numberline {4.1.1}Die Initialisierung}{6}
\contentsline {subsection}{\numberline {4.2}Beschleunigung der Welle}{7}
\contentsline {subsection}{\numberline {4.3}Die Interrupt-Funktionen}{7}
\contentsline {subsection}{\numberline {4.4}Die Bremslogik}{7}
\contentsline {subsection}{\numberline {4.5}Implementierung der Servo Bremsregelung}{9}
\contentsline {section}{\numberline {A}Quellcode}{10}
