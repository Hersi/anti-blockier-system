#define SERVO_SCHLIESSEN       23500; // 1.56 Millisekunden
#define SERVO_OFFEN            30000; // 1.76 Millisekunden
#define SERVO_MITTE            720000 // 20 Millisekunden

// Servor PIN
#define SERVO_PIN              5

// Interrupt PINS
#define RAD_INTERRUPT_PIN      8
#define MOTOR_INTERRUPT_PIN    7

// Interrupt Nummer
#define RAD_INTERRUPT_NR       3
#define MOTOR_INTERRUPT_NR     2

// Zeit des letzten Interrupts des Rades
unsigned long lastRadInterrupt = 0;
// Zeit des letzten Interrupts des Motors
unsigned long lastMotorInterrupt = 0;
// Aktuelle Zeit für RadInterrupt
unsigned long currentMillisRadInterr = millis();
// vorangegangene Zeit des RadInterrupt
unsigned long prevMillisRadInterr = currentMillisRadInterr;

// Hilfsvariable zum Speichern von currentMillisRadInterr - currentMillisRadInterr
int j = 0;
// Hilfsvariable für das überprüfen auf Taste 's' oder 'b'
bool sb = false;
// Hilfsvariable zum Wechseln der Zustände des Servos
bool b = true;
// Hilfsvariable zum setzen der Bremse
bool bremse = false;

void setup(){
  Serial.begin(9600);
  // Initialisieren der PINS
  pinMode(SERVO_PIN, OUTPUT);
  pinMode(RAD_INTERRUPT_PIN, INPUT);
  pinMode(MOTOR_INTERRUPT_PIN, INPUT);
  // Interrupt Funktionen initialisieren
  attachInterrupt(RAD_INTERRUPT_NR, RadInterrupt, RISING);
  attachInterrupt(MOTOR_INTERRUPT_NR, MotorInterrupt, RISING);
  // CoreTime Funktion initialisieren
  attachCoreTimerService(ServoPWM);
}

/*
* Mit dieser Interruptfunktion wird der Zeitpunkt der
* letzten aktivierung gespeichert. Daran erkennt man
* ob die Welle sich noch dreht.
*/
void MotorInterrupt(){
  lastMotorInterrupt = millis();
}

/*
* Mit dieser Interruptfunktion wird geprüft ob die 
* Bremse aktiviert/deaktiviert ist und die Zeit zum letzten 
* Interrupt gemessen...
*/
void RadInterrupt(){
      if(sb){
          bremse = true;
        }else{
           bremse = false;
        }  
    lastRadInterrupt = millis();
}


/*
* Loopfunktion...
*/
void loop(){
  // Einlesen der Taste 'b' für bremsen oder 's' für stop
  int str2 = Serial.read();
  // 98 = 'b'
  if(str2 == 98){
    sb = true;
    Serial.println("Bremsen!");
  // 115 = 's'
  }else if(str2 == 115){
    sb = false;
    Serial.println("Stop bremsen!");
  }
  // Wenn das Rad und die Welle stehen bleiben und dabei die Bremse
  // aktiviert ist, dann muss blockiert werden.
  unsigned long ms = millis();
  if ((ms - lastRadInterrupt >= 200) && (ms - lastMotorInterrupt >= 200) && sb ){
    bremse = true;
  // Bremse blockiert
  }else if (( ms - lastRadInterrupt >= 200 ) && ( ms -lastMotorInterrupt <= 200) && sb ){
    bremse = false; 
  // Rad rutscht
  }else if (( ms - lastMotorInterrupt >= 200 ) && ( ms - lastRadInterrupt <= 200) && sb ){
    bremse = false;
  }else if (!sb){
    bremse = false;
  }
}

/*
* Steuerung der Servo-PWM (50hz)
*/
uint32_t ServoPWM( uint32_t time ){
  static int pwm = 0;
  // Wechsel zwischen HIGH und LOW Signale
  if(b){
    digitalWrite(SERVO_PIN, HIGH);
    b = false;
    if(bremse){
      // PWM speichern für die Subtratktion zum Erreichen der 50Hz Frequenz
       pwm = SERVO_SCHLIESSEN;
    }else{
       // Bremse öffnen
       pwm = SERVO_OFFEN;
    }
  }else{
    digitalWrite(SERVO_PIN, LOW);
    b = true;
    return time + CORE_TICK_RATE + 720000 - pwm;
  }
  return time + CORE_TICK_RATE + pwm ;
}

